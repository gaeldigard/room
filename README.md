# 📽 Room
## Synchronized video watching with friends

Simple node.js app to watch synchronized video in browser, with a built-in
chat. Does not not provide any authentication layer.

Features :
* Built-in chat with nickname input
* ``.mp4`` files listing with thumbnail
* ``.mp4`` and YouTube video 
* Synchronized Play / Pause / Seek To 
* User login while other watching video, he'll take it on the fly
* ``!list`` to get logged-in users

Now supports : 
 * .mp4 videos by native HTML tag
 * YouTube videos
 
 ### 1/ Install
 * Install node.js (official [https://nodejs.org](https://nodejs.org/en/))
 * Clone repo, checkout master
 * Run ``npm install``
 * Move some ``.mp4`` videos in ``public/videos`` (not required)
 * Run ``node server.js`` or ``nohup node server.js`` to run it forever
 * Access via ``http://localhost:8080``
 
 ### 2/ Usage
 * Type your nickname bottom right of the screen, press return
 * Type messages, press return
 * Click the 3 lines icons to see files in ``videos/`` folder
 * Click one of them or past YouTube URL in the top input
 
 ### 3/ Improvements
 * Make it multi-lang
 * Make it mobile-friendly
 