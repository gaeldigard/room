/**
 * 06 Avril 2020
 * Gaël DIGARD
 *
 */

var YouTube = {};
YouTube.player = null;
YouTube.id = null;
YouTube.state = null;
YouTube.durationVideo = null;
YouTube.currentTime = null;

/**
 * On set a YT video
 */
socket.on('youtube_create', function (youtube_id) {
    YouTube.id = youtube_id;
    YouTubeLoadAPI();
});



/**
 * On change YTPlayer state
 */
socket.on('youtube_control', function (payload) {

    if (payload.state == 1) {
        YouTubePlayVideo()
        write_log(payload.username + ' a play la vidéo', 'log-video');

    }
    else {
        YouTubePauseVideo()
        write_log(payload.username + ' a pause la vidéo', 'log-video');

    }

});


/**
 * On set position on progress bar
 */
socket.on('youtube_position', function (payload) {

    // Everybody must know
    write_log(payload.username + ' a déplacé la vidéo dans le temps', 'log-video');

    // Set YTPlayer at correct time
    YouTube.player.seekTo(payload.position, true)

    // Set progress bar position
    progressBar.setAttribute('value', (payload.position / YouTube.durationVideo * 100).toString());
});


function YouTubeLoadAPI() {

    // First video loaded, inject script tag and API
    if (YouTube.player == null) {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
    // next time, just update video id
    else {
        // onYouTubeIframeAPIReady()
        YouTube.player.loadVideoById(YouTube.id);
        console.log('loadVideoById')
        // async Youtube.player
        while( YouTube.player != null) {
            console.log('into while')
            console.log(YouTube.player)
             onStartYouTubeVideo();
            break;
        }
    }
}

function setYouTubeVideo(url) {
    var url = $('#youtube_url').val();
    if (url != undefined || url != '') {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        // valid YT url
        if (match && match[2].length == 11) {

            pauseMedia();
            hideMedia();
            YouTube.id = match[2];

            socket.emit('youtube_create', YouTube.id)
            YouTubeLoadAPI();


        }
        else {
            alert('Fais gaffe putain, l\'URL n\'est pas bonne !');
        }
    }


}

function onYouTubeIframeAPIReady() {
    YouTube.player = new YT.Player('youtube_player', {
        height: '100%',
        width: '100%',
        videoId: YouTube.id,
        playerVars: {'autoplay': 1, 'controls': 0},
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    })
}

/**
 * When player ready
 * @param event
 */
function onPlayerReady(event) {
    event.target.playVideo();
    onStartYouTubeVideo()
}


/**
 * Set various var useful for socket communication
 * Like YouTube.state, YouTube.durationVideo
 * Close panel too and set proper pause icon
 * Run the interval to refresh progressBar
 */
function onStartYouTubeVideo() {
    YouTube.state = 1;
    play.setAttribute('src', 'img/pause.png');
    $('.close_files').click();

    while( undefined != YouTube.player.getDuration() ) {

            YouTube.durationVideo = YouTube.player.getDuration();

        $('#video_overlay').removeClass('hidden')
        videoInterval = setInterval(function () {
            YouTube.currentTime = YouTube.player.getCurrentTime();
            progressBar.setAttribute('value', (YouTube.currentTime / YouTube.durationVideo * 100).toString())

            //update current time on server to broadcast after to the new users
            socket.emit('update_current_time', {position: YouTube.currentTime, state: YouTube.state});
        }, 1000)
    break;
    }
}

function onPlayerStateChange(event) {
}

function YouTubePauseVideo() {
    YouTube.player.pauseVideo();
    YouTube.state = 0;
    play.setAttribute('src', 'img/play.png');
}

function YouTubeStopVideo() {
    YouTube.player.stopVideo();
}

function YouTubePlayVideo() {
    YouTube.player.playVideo();
    YouTube.state = 1;
    play.setAttribute('src', 'img/pause.png');
}


play.addEventListener('click', function () {

    // YT management
    if (YouTube.player != null) {
        if (YouTube.state != 1) {
            YouTubePlayVideo()

        }
        else {
            YouTubePauseVideo()

        }
        socket.emit('youtube_control', {
            state: YouTube.state,
            username: username,
            currentTime: YouTube.player.getCurrentTime()
        });
    }

});


/**
 * On progress bar click, change "position"
 */
progressBar.addEventListener('click', function (e) {
    var max = $(progressBar).width(); //Get width element
    var pos = e.pageX - $(progressBar).offset().left; //Position cursor
    var progressionValue = Math.round(pos / max * 100); // Round %
    if (progressionValue > 100) {
        var progressionValue = 100;
    }

    if (YouTube.id != null) {
        // store position
        YouTube.currentTime = (progressionValue * YouTube.durationVideo / 100);

        // set video position
        YouTube.player.seekTo(YouTube.currentTime, true);

        // let's tell to everybody
        socket.emit('youtube_position', {
            username: username,
            position: YouTube.currentTime
        });

    }

    // update progress bar
    $(progressBar).val(progressionValue);
});