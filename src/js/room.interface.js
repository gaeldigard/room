/**
 * 28 Mars 2020
 * Gaël DIGARD
 *
 */
var socket = io.connect();

var play = document.querySelector('.play img');
var progressBar = document.querySelector('progress');
var filesButton = document.querySelector('.files img');
const videoFolder = 'videos/';
var activityInterval = setInterval(clearActivity, 3000);
var videoInterval = null;
var username = null;

/*
* Events
*/
jQuery(document).ready(function () {

    // set message box focused
    $('#content_username').focus();
    setMessagesListHeight();

    // ask for files list
    socket.emit('files_list', '')

    // After few milliseconds, start to erase "stranger", every 250ms
    setTimeout( function(){
      const userNameEl = jQuery('#username');
      var stranger = userNameEl.text()
      stranger = Array.from(stranger)
      var index = stranger.length;

      setInterval(function(){
        if(stranger.length){
           stranger.pop()
           console.log(stranger)
           userNameEl.html(Array.from(stranger));

         }
         // when stranger is erased, set and input
         else{

           // set input only once
           if(jQuery('#username_input').length==0){
             userNameEl.html('<input id="username_input" onkeydown="if (event.keyCode == 13) saveUsername()"/>').focus();
             jQuery('#username_input').focus();
            }
         }
      }, 250)
    }, 500)
});



$(window).resize(function () {
    setMessagesListHeight();
});

/**
 * On click on overlay, let's inform to rather use external controls
 */
$('#video_overlay').click(function () {
    $(this).addClass('clicked')
    setTimeout(function () {
        $('#video_overlay').removeClass('clicked')
        $('#controls').addClass('highlight')
        setTimeout(function () {
            $('#controls').removeClass('highlight')
        }, 500);
    }, 500)
});

/**
 * On file click
 */
$("#files_list ol").on("click", "li", function () {

    var file_name = $(this).text();
    changeVideoSource(file_name)

    // end to everybody
    socket.emit('change_source', file_name);
});


/**
 * On close any panel.
 */
$('.close_parent').click(function () {
    $(this).parent().removeClass('active')
});

/**
 * Now playing receiving
 */
socket.on('playing_now', function (payload) {
    console.log(payload)
    if(payload.type == 'youtube'){
        YouTube.id = payload.videoRef;
        YouTube.currentTime = payload.currentTime;
        YouTube.state = payload.currentState;
        YouTubeLoadAPI();

        // async YouTube.player so we wait
        while(YouTube.player != null ){
            YouTube.player.seekTo(YouTube.currentTime)
            break;
        }

    }
});


/**
 * On receive files list
 */
socket.on('files_list', function (files_list) {
    for (var i = 0; i < files_list.length; i++) {
        var file = files_list[i];
        if (file.indexOf('.mp4') >= 0)
            $('#files_list ol').append('<li>' + file + '</li>');
    }
})


/**
 * Chat conversation + login/out logs
 */
socket.on('message', function (message) {
    if (message.type == 'chat') {

        var my_message = false;
        var message_string = false;

        if (message.user == username) {

            // set text on the right
            var my_message = 'my_message';
            // dont show you own name
            message_string = message.content;
        }
        else {
            message_string = '<div class="username">' + message.user + '</div>' + message.content;
        }
        $('#messages ul').append('<li class="' + my_message + ' ' + message.type + '">' + message_string + '</li>')

    }
    else if (message.type == 'log') {
        $('#messages ul').append('<li class="log">' + message.content + '</li>')
    }
    updateScroll('messages');
});



/**
 * On someone's typing
 */
socket.on('activity', function (username) {
    $('#activity').html('🖊️ ' + username + ' est en train d\'écrire...')
});

/**
 * On users list received
 */
socket.on('list', function (list) {
    var msg = 'Les faquins connectés sont les suivants : ';
    for( var i = 0;i<list.length;i++){
        msg += list[i].username + ', '
    }
    msg = msg.replace(/,\s*$/, "");
    write_log( msg )
}); 




/**
 * Various functions
 */

function write_log(message, className) {
    if (className == undefined)
        className = 'log'
    $('#messages ul').append('<li class="' + className + '">' + message + '</li>')
}


/**
 * Set messages list height, according to bottom zone height
 */
function setMessagesListHeight() {
    var document_height = document.documentElement.scrollHeight;
    var bottomZone_height = jQuery('#bottom_zone').height();
    jQuery('#messages').css({
        height: parseInt(document_height - bottomZone_height - 160)
    });
}

/* Scroll to the bottom*/
function updateScroll(id) {
    var element = document.getElementById(id);
    element.scrollTop = element.scrollHeight;
}

/**
 * Someone is typing...
 */
function sendActivity() {
    socket.emit('activity', username);
}

/**
 * Empty the "someone is typing" zone
 */
function clearActivity() {
    $('#activity').empty();
}

/**
 * Save and broadcast the username
 */
function saveUsername() {
    usernameInputEl = $('#username_input')
    username = usernameInputEl.val();
    socket.emit('new_user', username);
    setTimeout(function(){
      $('blockquote').fadeOut('slow', function(){
        $('blockquote').remove();
        $('.two').removeClass('hidden');
        $('#content_message, #controls').removeClass('hidden')
        $('#content_message').focus();

        $('blockquote').text('Welcome, ' + username);
        socket.emit('whats_playing_now', username);
      })


    }, 250)
}

/**
 * Send message
 * @param key
 */
function sendMessage() {
    var content_message = $('#content_message').val().trim();

    if(content_message == '!list'){
        socket.emit('list_asked');

    }
    else{
        socket.emit('message', content_message)
    }

    $('#content_message').val('');

}

filesButton.addEventListener('click', function () {
    $('#files_list').toggleClass('active');
});
