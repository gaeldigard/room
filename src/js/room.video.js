/**
 * 06 Avril 2020
 * Gaël DIGARD
 *
 */
var media = document.querySelector('video');
var source = document.querySelector('source');
var controls = document.querySelector('.controls');
var MP4Video = {};
MP4Video.state = null;
MP4Video.durationVideo = null;
MP4Video.currentTime = null;


/**
 * On receive a video source change
 */
socket.on('change_source', function (file_name) {
    changeVideoSource(file_name);
})

/**
 * On control received (play/pause)
 */
socket.on('video_control', function (payload) {

    // show a log
    $('#messages ul').append('<li class="log-video">' + payload.username + ' a ' + payload.action + ' la video</li>')

    // re-sync time
    media.currentTime = payload.currentTime;

    // execute action
    if (payload.action == 'play') {
        playMedia()
    }
    else if (payload.action == 'pause') {
        pauseMedia()
    }

})

/**
 * Media playing

 */

/**
 * On progress bar click, change "position"
 */
progressBar.addEventListener('click', function (e) {
    var max = $(progressBar).width(); //Get width element
    var pos = e.pageX - $(progressBar).offset().left; //Position cursor
    var progressionValue = Math.round(pos / max * 100); // Round %
    if (progressionValue > 100) {
        var progressionValue = 100;
    }

    if (YouTube.id == null) {
        MP4Video.currentTime = (progressionValue * MP4Video.durationVideo / 100);
        media.currentTime = MP4Video.currentTime;
        // let's tell to everybody
        socket.emit('video_position', {
            username: username,
            position: MP4Video.currentTime
        });

    }


    // update progress bar
    $(progressBar).val(progressionValue);
});

play.addEventListener('click', function () {

    // YT management
    if (YouTube.player == null) {
        if (media.paused) {
            var action = 'play'
        }
        else {
            var action = 'pause'
        }
        socket.emit('video_control', {username: username, action: action, currentTime: media.currentTime})

        playPauseMedia()
    }

});

/**
 * Toggle between pause and play
 */
function playPauseMedia() {
    var state = null;
    if (media.paused) {
        playMedia()
        state = 1;
    } else {
        pauseMedia()
        state = 0;
    }

    // reset ID
    YouTube.id=null;

    MP4Video.durationVideo = media.duration;

    videoInterval = setInterval(function () {
        MP4Video.currentTime = media.currentTime;

        progressBar.setAttribute('value', (MP4Video.currentTime / MP4Video.durationVideo * 100).toString())
        //update current time on server to broadcast after to the new users

        socket.emit('update_current_time', {position: MP4Video.currentTime, state: state});
    }, 1000)
}

/**
 * Hide HTML5 video
 */
function hideMedia() {
    $('#player').fadeOut();
}

/**
 * Show HTML5 video
 */
function showMedia() {
    $('#player').fadeIn();
}

/**
 * Pause video
 */
function pauseMedia() {
    play.setAttribute('src', 'img/play.png');
    media.pause();
}

/**
 * Play video
 */
function playMedia() {
    play.setAttribute('src', 'img/pause.png');
    media.play();
}


/**
 * Change video source by a new one
 * @param file_name
 */
function changeVideoSource(file_name) {

    // reset YT
    YouTube.id = null;
    YouTube.player = null;

    media.pause();
    source.setAttribute('src', videoFolder + file_name);
    var thumb = file_name.split('.').slice(0, -1).join('.')
    media.setAttribute('poster', videoFolder + thumb + '.jpg');
    media.load();


}

