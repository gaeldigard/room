/**
 * 28 Mars 2020
 * Gaël DIGARD
 *
 */
var socket = io.connect();

var play = document.querySelector('.play img');
var progressBar = document.querySelector('progress');
var filesButton = document.querySelector('.files img');
const videoFolder = 'videos/';
var activityInterval = setInterval(clearActivity, 3000);
var videoInterval = null;
var username = null;

/*
* Events
*/
jQuery(document).ready(function () {

    // set message box focused
    $('#content_username').focus();
    setMessagesListHeight();

    // ask for files list
    socket.emit('files_list', '')

    // After few milliseconds, start to erase "stranger", every 250ms
    setTimeout( function(){
      const userNameEl = jQuery('#username');
      var stranger = userNameEl.text()
      stranger = Array.from(stranger)
      var index = stranger.length;

      setInterval(function(){
        if(stranger.length){
           stranger.pop()
           console.log(stranger)
           userNameEl.html(Array.from(stranger));

         }
         // when stranger is erased, set and input
         else{

           // set input only once
           if(jQuery('#username_input').length==0){
             userNameEl.html('<input id="username_input" onkeydown="if (event.keyCode == 13) saveUsername()"/>').focus();
             jQuery('#username_input').focus();
            }
         }
      }, 250)
    }, 500)
});



$(window).resize(function () {
    setMessagesListHeight();
});

/**
 * On click on overlay, let's inform to rather use external controls
 */
$('#video_overlay').click(function () {
    $(this).addClass('clicked')
    setTimeout(function () {
        $('#video_overlay').removeClass('clicked')
        $('#controls').addClass('highlight')
        setTimeout(function () {
            $('#controls').removeClass('highlight')
        }, 500);
    }, 500)
});

/**
 * On file click
 */
$("#files_list ol").on("click", "li", function () {

    var file_name = $(this).text();
    changeVideoSource(file_name)

    // end to everybody
    socket.emit('change_source', file_name);
});


/**
 * On close any panel.
 */
$('.close_parent').click(function () {
    $(this).parent().removeClass('active')
});

/**
 * Now playing receiving
 */
socket.on('playing_now', function (payload) {
    console.log(payload)
    if(payload.type == 'youtube'){
        YouTube.id = payload.videoRef;
        YouTube.currentTime = payload.currentTime;
        YouTube.state = payload.currentState;
        YouTubeLoadAPI();

        // async YouTube.player so we wait
        while(YouTube.player != null ){
            YouTube.player.seekTo(YouTube.currentTime)
            break;
        }

    }
});


/**
 * On receive files list
 */
socket.on('files_list', function (files_list) {
    for (var i = 0; i < files_list.length; i++) {
        var file = files_list[i];
        if (file.indexOf('.mp4') >= 0)
            $('#files_list ol').append('<li>' + file + '</li>');
    }
})


/**
 * Chat conversation + login/out logs
 */
socket.on('message', function (message) {
    if (message.type == 'chat') {

        var my_message = false;
        var message_string = false;

        if (message.user == username) {

            // set text on the right
            var my_message = 'my_message';
            // dont show you own name
            message_string = message.content;
        }
        else {
            message_string = '<div class="username">' + message.user + '</div>' + message.content;
        }
        $('#messages ul').append('<li class="' + my_message + ' ' + message.type + '">' + message_string + '</li>')

    }
    else if (message.type == 'log') {
        $('#messages ul').append('<li class="log">' + message.content + '</li>')
    }
    updateScroll('messages');
});



/**
 * On someone's typing
 */
socket.on('activity', function (username) {
    $('#activity').html('🖊️ ' + username + ' est en train d\'écrire...')
});

/**
 * On users list received
 */
socket.on('list', function (list) {
    var msg = 'Les faquins connectés sont les suivants : ';
    for( var i = 0;i<list.length;i++){
        msg += list[i].username + ', '
    }
    msg = msg.replace(/,\s*$/, "");
    write_log( msg )
}); 




/**
 * Various functions
 */

function write_log(message, className) {
    if (className == undefined)
        className = 'log'
    $('#messages ul').append('<li class="' + className + '">' + message + '</li>')
}


/**
 * Set messages list height, according to bottom zone height
 */
function setMessagesListHeight() {
    var document_height = document.documentElement.scrollHeight;
    var bottomZone_height = jQuery('#bottom_zone').height();
    jQuery('#messages').css({
        height: parseInt(document_height - bottomZone_height - 160)
    });
}

/* Scroll to the bottom*/
function updateScroll(id) {
    var element = document.getElementById(id);
    element.scrollTop = element.scrollHeight;
}

/**
 * Someone is typing...
 */
function sendActivity() {
    socket.emit('activity', username);
}

/**
 * Empty the "someone is typing" zone
 */
function clearActivity() {
    $('#activity').empty();
}

/**
 * Save and broadcast the username
 */
function saveUsername() {
    usernameInputEl = $('#username_input')
    username = usernameInputEl.val();
    socket.emit('new_user', username);
    setTimeout(function(){
      $('blockquote').fadeOut('slow', function(){
        $('blockquote').remove();
        $('.two').removeClass('hidden');
        $('#content_message, #controls').removeClass('hidden')
        $('#content_message').focus();

        $('blockquote').text('Welcome, ' + username);
        socket.emit('whats_playing_now', username);
      })


    }, 250)
}

/**
 * Send message
 * @param key
 */
function sendMessage() {
    var content_message = $('#content_message').val().trim();

    if(content_message == '!list'){
        socket.emit('list_asked');

    }
    else{
        socket.emit('message', content_message)
    }

    $('#content_message').val('');

}

filesButton.addEventListener('click', function () {
    $('#files_list').toggleClass('active');
});

/**
 * 06 Avril 2020
 * Gaël DIGARD
 *
 */
var media = document.querySelector('video');
var source = document.querySelector('source');
var controls = document.querySelector('.controls');
var MP4Video = {};
MP4Video.state = null;
MP4Video.durationVideo = null;
MP4Video.currentTime = null;


/**
 * On receive a video source change
 */
socket.on('change_source', function (file_name) {
    changeVideoSource(file_name);
})

/**
 * On control received (play/pause)
 */
socket.on('video_control', function (payload) {

    // show a log
    $('#messages ul').append('<li class="log-video">' + payload.username + ' a ' + payload.action + ' la video</li>')

    // re-sync time
    media.currentTime = payload.currentTime;

    // execute action
    if (payload.action == 'play') {
        playMedia()
    }
    else if (payload.action == 'pause') {
        pauseMedia()
    }

})

/**
 * Media playing

 */

/**
 * On progress bar click, change "position"
 */
progressBar.addEventListener('click', function (e) {
    var max = $(progressBar).width(); //Get width element
    var pos = e.pageX - $(progressBar).offset().left; //Position cursor
    var progressionValue = Math.round(pos / max * 100); // Round %
    if (progressionValue > 100) {
        var progressionValue = 100;
    }

    if (YouTube.id == null) {
        MP4Video.currentTime = (progressionValue * MP4Video.durationVideo / 100);
        media.currentTime = MP4Video.currentTime;
        // let's tell to everybody
        socket.emit('video_position', {
            username: username,
            position: MP4Video.currentTime
        });

    }


    // update progress bar
    $(progressBar).val(progressionValue);
});

play.addEventListener('click', function () {

    // YT management
    if (YouTube.player == null) {
        if (media.paused) {
            var action = 'play'
        }
        else {
            var action = 'pause'
        }
        socket.emit('video_control', {username: username, action: action, currentTime: media.currentTime})

        playPauseMedia()
    }

});

/**
 * Toggle between pause and play
 */
function playPauseMedia() {
    var state = null;
    if (media.paused) {
        playMedia()
        state = 1;
    } else {
        pauseMedia()
        state = 0;
    }

    // reset ID
    YouTube.id=null;

    MP4Video.durationVideo = media.duration;

    videoInterval = setInterval(function () {
        MP4Video.currentTime = media.currentTime;

        progressBar.setAttribute('value', (MP4Video.currentTime / MP4Video.durationVideo * 100).toString())
        //update current time on server to broadcast after to the new users

        socket.emit('update_current_time', {position: MP4Video.currentTime, state: state});
    }, 1000)
}

/**
 * Hide HTML5 video
 */
function hideMedia() {
    $('#player').fadeOut();
}

/**
 * Show HTML5 video
 */
function showMedia() {
    $('#player').fadeIn();
}

/**
 * Pause video
 */
function pauseMedia() {
    play.setAttribute('src', 'img/play.png');
    media.pause();
}

/**
 * Play video
 */
function playMedia() {
    play.setAttribute('src', 'img/pause.png');
    media.play();
}


/**
 * Change video source by a new one
 * @param file_name
 */
function changeVideoSource(file_name) {

    // reset YT
    YouTube.id = null;
    YouTube.player = null;

    media.pause();
    source.setAttribute('src', videoFolder + file_name);
    var thumb = file_name.split('.').slice(0, -1).join('.')
    media.setAttribute('poster', videoFolder + thumb + '.jpg');
    media.load();


}


/**
 * 06 Avril 2020
 * Gaël DIGARD
 *
 */

var YouTube = {};
YouTube.player = null;
YouTube.id = null;
YouTube.state = null;
YouTube.durationVideo = null;
YouTube.currentTime = null;

/**
 * On set a YT video
 */
socket.on('youtube_create', function (youtube_id) {
    YouTube.id = youtube_id;
    YouTubeLoadAPI();
});



/**
 * On change YTPlayer state
 */
socket.on('youtube_control', function (payload) {

    if (payload.state == 1) {
        YouTubePlayVideo()
        write_log(payload.username + ' a play la vidéo', 'log-video');

    }
    else {
        YouTubePauseVideo()
        write_log(payload.username + ' a pause la vidéo', 'log-video');

    }

});


/**
 * On set position on progress bar
 */
socket.on('youtube_position', function (payload) {

    // Everybody must know
    write_log(payload.username + ' a déplacé la vidéo dans le temps', 'log-video');

    // Set YTPlayer at correct time
    YouTube.player.seekTo(payload.position, true)

    // Set progress bar position
    progressBar.setAttribute('value', (payload.position / YouTube.durationVideo * 100).toString());
});


function YouTubeLoadAPI() {

    // First video loaded, inject script tag and API
    if (YouTube.player == null) {
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
    // next time, just update video id
    else {
        // onYouTubeIframeAPIReady()
        YouTube.player.loadVideoById(YouTube.id);
        console.log('loadVideoById')
        // async Youtube.player
        while( YouTube.player != null) {
            console.log('into while')
            console.log(YouTube.player)
             onStartYouTubeVideo();
            break;
        }
    }
}

function setYouTubeVideo(url) {
    var url = $('#youtube_url').val();
    if (url != undefined || url != '') {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        // valid YT url
        if (match && match[2].length == 11) {

            pauseMedia();
            hideMedia();
            YouTube.id = match[2];

            socket.emit('youtube_create', YouTube.id)
            YouTubeLoadAPI();


        }
        else {
            alert('Fais gaffe putain, l\'URL n\'est pas bonne !');
        }
    }


}

function onYouTubeIframeAPIReady() {
    YouTube.player = new YT.Player('youtube_player', {
        height: '100%',
        width: '100%',
        videoId: YouTube.id,
        playerVars: {'autoplay': 1, 'controls': 0},
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    })
}

/**
 * When player ready
 * @param event
 */
function onPlayerReady(event) {
    event.target.playVideo();
    onStartYouTubeVideo()
}


/**
 * Set various var useful for socket communication
 * Like YouTube.state, YouTube.durationVideo
 * Close panel too and set proper pause icon
 * Run the interval to refresh progressBar
 */
function onStartYouTubeVideo() {
    YouTube.state = 1;
    play.setAttribute('src', 'img/pause.png');
    $('.close_files').click();

    while( undefined != YouTube.player.getDuration() ) {

            YouTube.durationVideo = YouTube.player.getDuration();

        $('#video_overlay').removeClass('hidden')
        videoInterval = setInterval(function () {
            YouTube.currentTime = YouTube.player.getCurrentTime();
            progressBar.setAttribute('value', (YouTube.currentTime / YouTube.durationVideo * 100).toString())

            //update current time on server to broadcast after to the new users
            socket.emit('update_current_time', {position: YouTube.currentTime, state: YouTube.state});
        }, 1000)
    break;
    }
}

function onPlayerStateChange(event) {
}

function YouTubePauseVideo() {
    YouTube.player.pauseVideo();
    YouTube.state = 0;
    play.setAttribute('src', 'img/play.png');
}

function YouTubeStopVideo() {
    YouTube.player.stopVideo();
}

function YouTubePlayVideo() {
    YouTube.player.playVideo();
    YouTube.state = 1;
    play.setAttribute('src', 'img/pause.png');
}


play.addEventListener('click', function () {

    // YT management
    if (YouTube.player != null) {
        if (YouTube.state != 1) {
            YouTubePlayVideo()

        }
        else {
            YouTubePauseVideo()

        }
        socket.emit('youtube_control', {
            state: YouTube.state,
            username: username,
            currentTime: YouTube.player.getCurrentTime()
        });
    }

});


/**
 * On progress bar click, change "position"
 */
progressBar.addEventListener('click', function (e) {
    var max = $(progressBar).width(); //Get width element
    var pos = e.pageX - $(progressBar).offset().left; //Position cursor
    var progressionValue = Math.round(pos / max * 100); // Round %
    if (progressionValue > 100) {
        var progressionValue = 100;
    }

    if (YouTube.id != null) {
        // store position
        YouTube.currentTime = (progressionValue * YouTube.durationVideo / 100);

        // set video position
        YouTube.player.seekTo(YouTube.currentTime, true);

        // let's tell to everybody
        socket.emit('youtube_position', {
            username: username,
            position: YouTube.currentTime
        });

    }

    // update progress bar
    $(progressBar).val(progressionValue);
});