const express = require('express')
const app = express()
var server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    ent = require('ent'),
    fs = require('fs');

const videoFolder = './public/videos/';

// Contains a list of connected users
var currentUsers = [];

/**
 * Object about what's now playing
 * @type {{}}
 */
var nowPlaying = {};
nowPlaying.type = null;
nowPlaying.currentTime = null;
nowPlaying.currentState = null;
nowPlaying.videoRef = null; // can be file name or Youtube ID

console.log('Ready...')

// Chargement de la page index.html
app.get('/', function (req, res) {
    res.sendfile(__dirname + '/public/index.html');
});

app.use(express.static(__dirname + '/public'));
// Chargement de socket.io
var io = require('socket.io').listen(server);


io.sockets.on('connection', function (socket, pseudo) {

    console.log('Some random dude(ette) logged in with id : ' + socket.id);





    /* When someone ask the users list */
    socket.on('list_asked', function () {
        console.log(currentUsers)
        socket.emit('list', currentUsers)
    });

    /* When someone log in, he asks what we are playing */
    socket.on('whats_playing_now', function () {
        socket.emit('playing_now', nowPlaying)
    });

    /* Called every second to update video position */
    socket.on('update_current_time', function (payload) {
        nowPlaying.currentTime = payload.position;
        nowPlaying.currentState = payload.state;
        console.log('nowPlaying.currentTime = ' + nowPlaying.currentTime)
        console.log('nowPlaying.currentState = ' + nowPlaying.currentState)

    })

    // position control on progress bar
    socket.on('youtube_position', function (payload) {

        socket.broadcast.emit('youtube_position', payload);
        nowPlaying.currentTime = payload.position;
        console.log(nowPlaying.currentTime)
    });

    // play/pause/position on YT player
    socket.on('youtube_control', function (payload) {

        socket.broadcast.emit('youtube_control', payload);
        nowPlaying.currentState = payload.state;
        nowPlaying.currentTime = payload.currentTime;
        console.log(nowPlaying.currentState)
        console.log(nowPlaying.currentTime)

    });

    socket.on('youtube_create', function (YouTubeId) {
        socket.broadcast.emit('youtube_create', YouTubeId);
        nowPlaying.type = 'youtube';
        nowPlaying.videoRef = YouTubeId;
        console.log(nowPlaying.type)
        console.log(nowPlaying.videoRef)
    });


    // ask for directory content
    socket.on('files_list', function (payload) {
        socket.emit('files_list', fs.readdirSync(videoFolder));
    });

    // change video source
    socket.on('change_source', function (payload) {
        socket.broadcast.emit('change_source', payload);
        nowPlaying.type = 'mp4';
        nowPlaying.videoRef = payload.file_name;
        console.log(nowPlaying.type)
        console.log(nowPlaying.videoRef)

    });

    // play/pause, set position
    socket.on('video_control', function (payload) {
        socket.broadcast.emit('video_control', payload);
        nowPlaying.currentState = payload.action;
        nowPlaying.currentTime = payload.currentTime;
        console.log(nowPlaying.currentState)
        console.log(nowPlaying.currentTime)
    });

    // Dès qu'on nous donne un pseudo, on le stocke en variable de session
    socket.on('new_user', function (pseudo) {
        socket.pseudo = ent.encode(pseudo);

        socket.broadcast.emit('message', {
            type: 'log',
            content: socket.pseudo + ' vient d\'arriver'
        });

        console.log(socket.pseudo + ' set his username')

        // store
        var o = {};
        o.socket = socket.id;
        o.username = pseudo
        currentUsers.push(o);
        console.log(currentUsers)


    });

    // message received, broadcast it
    socket.on('message', function (message) {
        message = ent.encode(message);
        // io emit to everybody, including sender
        // socket.broadcast.emit to everybody except sender
        io.emit('message', {
            type: 'chat',
            user: socket.pseudo,
            content: message
        });
    });

    // someone is typing
    socket.on('activity', function (username) {
        socket.broadcast.emit('activity', username);
    });

    // someone leaves
    socket.on('disconnect', function () {

        for( var i = 0;i<currentUsers.length;i++) {
            if( socket.id == currentUsers[i].socket ){

                console.log( currentUsers[i].username + ' just left');

                io.emit('message', {
                    type: 'log',
                    user: currentUsers[i].username,
                    content: currentUsers[i].username + ' s\'est barré comme un fils de pute'
                });

                // remove from list
                currentUsers.splice(i);

            }
        }
        console.log('currentUsers size = ' + Object.keys(currentUsers).length);



        // no one's left, reset nowPlaying
        if (Object.keys(currentUsers).length == 0) {

            nowPlaying.type = null;
            nowPlaying.currentTime = null;
            nowPlaying.currentState = null;
            nowPlaying.videoRef = null;

            console.log('Everyone has left...')
        }
    });
});


server.listen(8080);
