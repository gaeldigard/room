'use strict';

var gulp      = require('gulp');
var sass      = require('gulp-sass');
var concat    = require('gulp-concat');
const minify  = require('gulp-minify');
sass.compiler = require('node-sass');

// files paths
var jsFiles   = './src/js/**/*.js',
    jsDest    = './public/js',
    cssFiles  = './src/sass/**/*.scss',
    cssDest   = './public/css';

/**
* Transpile SASS into compressed CSS.
*/
gulp.task('sass', function () {
    return gulp.src(cssFiles)
        .pipe(sass({
          outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest(cssDest));
});

/**
* Concat all JS files and minify them 
*/
gulp.task('scripts', function() {
    return gulp.src(jsFiles)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(minify())
        .pipe(gulp.dest(jsDest));
});


gulp.task('watch', function () {
    gulp.watch('./src/sass/**/*.scss', gulp.series('sass'));
    gulp.watch('./src/js/**/*.js', gulp.series('scripts'));
});
